import pickle


class ObjectData():
    def __init__(self, unserialized_obj, id):
        self.id = id
        self.data = self.serialize(unserialized_obj, id)

    @staticmethod
    def serialize(unserialized_obj, id):
        return pickle.dumps(unserialized_obj)
