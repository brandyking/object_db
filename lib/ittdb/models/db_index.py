from bplustree import BPlusTree
import db.db_config as config


class DbIndex:
    def __init__(self, filename):
        self.entries = BPlusTree(filename)

    def get_latest_id(self):
        latest = -1
        for entry in self.entries:
            if entry > latest:
                latest = entry
        return latest

    def increment_latest_id(self):
        return self.get_latest_id() + 1

    def get_offset(self, id):
        return self.entries.get(id)

    def get_next_offset(self):
        return self.get_entry_count() * config.DB_RECORD_SIZE_IN_BYTES

    def get_entry_count(self):
        count = len(self.entries)
        if count is not None:
            return count
        return 0

    def add_entry(self, id, offset):
        offset = str(offset)
        binary_offset = eval("b'" + offset + "'")
        self.entries[id] = binary_offset

    def close(self):
        self.entries.close()
