import pickle
from lib.ittdb.models.object_data import ObjectData
from lib.ittdb.models.db_index import DbIndex
import db.db_config as config


def save(obj):
    typename = type(obj).__name__
    id = add_index_entry(typename)
    db_data = encapsulate_data(obj, id)
    save_as(typename, db_data)
    return id


def find(typename, search_criteria):
    try:
        index = get_index(get_idx_filename(typename))
        offset = convert_binary_to_int(index.entries.get(search_criteria.id))
        return get_db_record_at_offset(get_db_filename(typename), offset)
    finally:
        if index:
            index.close()


def add_index_entry(typename):
    try:
        index = get_index(get_idx_filename(typename))
        new_id = index.increment_latest_id()
        index.add_entry(new_id, index.get_next_offset())
        index.close()
        return new_id
    finally:
        if index:
            index.close()


def get_index(filename):
    try:
        with open(filename, 'rb') as file:
            data = file.read()
            index = pickle.loads(data)
        return index
    except:
        return DbIndex(filename)


def encapsulate_data(obj, id):
    obj_data = ObjectData(obj, id)
    return obj_data


def save_as(typename, data):
    db_object = serialize(data)
    db_filename = get_db_filename(typename)
    append_to_file(db_object, db_filename)


def get_db_record_at_offset(filename, offset):
    with open(filename, 'rb') as file:
        file.seek(offset)
        chunk = file.read(config.DB_RECORD_SIZE_IN_BYTES)
        serialized_obj = pickle.loads(chunk)
        return pickle.loads(serialized_obj.data)


def serialize(data):
    pickled_data = pickle.dumps(data)
    return pad(pickled_data)


def pad(binary_obj):
    return binary_obj.ljust(config.DB_RECORD_SIZE_IN_BYTES, b'\0')


def append_to_file(obj, filename):
    with open(filename, 'ab') as file:
        file.write(obj)


def get_db_filename(typename):
    return config.DB_FILES_PREFIX + typename + config.DB_FILES_SUFFIX


def get_idx_filename(typename):
    return config.IDX_FILES_PREFIX + typename + config.IDX_FILES_SUFFIX


def convert_binary_to_int(binary_num):
    str = binary_num.decode("utf-8")
    return int(str)
