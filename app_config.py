import argparse


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('-a', '--action', type=str, dest="action", metavar='$ACTION', required=True,
                        choices=['test', 'create', 'find', 'update', 'delete'],
                        help="The Object_DB action to perform.")
    parser.add_argument('-t', '--type', type=str, dest="type", metavar='$TYPE', default=None,
                        choices=['Customer', 'customer'],
                        help="The type of the object to do CRUD action on/with in the Object_DB.")
    parser.add_argument('-i', '--id', type=str, dest="id", metavar='$ID', default=None,
                        help="The ID of the object in the Object_DB")
    parser.add_argument('-p', '--props', type=str, dest="props", metavar='$PROPS', default=None,
                        help="Object properties in form '{key1:val1, key2:val2, ...}'")
    args = parser.parse_args()
    return args


def check_args(args, options):
    result = True
    for option in options:
        if getattr(args, option) is None:
            print('Error: --{} option cannot be None to execute {} action.'.format(option, args.action))
            result = False
    return result
