import lib.ittdb.ittdb as db


def save(obj):
    try:
        return db.save(obj)
    except:
        print('Unable to save new {}'.format(type(obj).__name__))


def find(typename, search_criteria):
    try:
        return db.find(typename, search_criteria)
    except:
        print('Unable to find {}'.format(typename))
