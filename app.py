import sys
import re
import app_config
import db.db as db
from db.models.search_criteria import SearchCriteria
from models.customer import Customer


def create_objects(args):
    property_values_list = parse_property_values(args)
    values_count = len(property_values_list)
    for value_index in range(0, values_count, 2):
        object_property_values = [property_values_list[value_index], property_values_list[value_index + 1]]
        create_object(args.type.capitalize(), object_property_values)
        

def parse_property_values(args):
    regex_for_property_values = ':\s?([\w\s,]+);|:\s?([\w\s,]+)$|:\s?([\w\s,]+)\]'
    regex_matches = re.findall(regex_for_property_values, args.props)
    property_values_list = \
        list(filter(filter_nulls, [value for match in regex_matches for value in filter(filter_nulls, match)]))
    return property_values_list


def create_object(typename, property_values):
    object_class = getattr(sys.modules[__name__], typename)
    new_object = object_class(*property_values)
    new_id = db.save(new_object)
    print('new {} ID: {}'.format(typename, new_id))


def print_props(obj):
    properties = [prop for prop in dir(obj) if prop[:2] != "__"]
    for prop in properties:
        print(prop + ":", getattr(obj, prop))


def filter_nulls(item):
    if item:
        return True
    return False


def print_error():
    print('Bad input command or argument. Please try again.')


if __name__ == "__main__":
    args = app_config.parse_args()

    try:
        # Invoke via: python app.py -a test
        if args.action == 'test':
            print('CLI is working')
        else:
            print_error()

        # Invoke like: python app.py -a create -t customer --props "name:Zeppo; address:NYC, NY"
        # or python app.py -a create -t customer --props "[name:Groucho; address:NYC, NY],[name:Harpo; address:NYC, NY],[name:Chico; address:NYC, NY]"
        if args.action == 'create' and app_config.check_args(args, ['type', 'props']):
            create_objects(args)
        else:
            print_error()

        # Invoke like: python app.py -a find -t customer --id 0
        if args.action == 'find' and app_config.check_args(args, ['type', 'id']):
            found_object = db.find(args.type.capitalize(), SearchCriteria(int(args.id)))
            print_props(found_object)
        else:
            print_error()
    except:
        print_error()
